FROM alpine:latest

WORKDIR ./ansible

COPY ./ ./

ENV PYTHONUNBUFFERED = 1

RUN apk add --update --no-cache openfortivpn --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing

RUN apk add --update --no-cache git openssh ansible

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python

RUN python3 -m ensurepip

RUN pip3 install --no-cache --upgrade pip setuptools envsubst

ENTRYPOINT ["/ansible"]
